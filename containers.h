#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <deque>
#include <string>
#include <iterator>
#include <utility> 
#include <algorithm>
#include <numeric>
#include <functional> 
#include <iomanip>
#include <set>

using namespace std;

class Employee {
private: string id, name, surname, departmentId, position;
public:
	Employee(const string, const string, const string, const string, const string);
	~Employee();
	string getID() const;
	string getname() const;
	string getsurname() const;
	string getdepartmentId() const;
	string getposition() const;
};

class HRMS{
public:
	deque<Employee> AllEmployees;
	map <const string, string> mapList;
	map <const string, double> mapSalary;

	void add(Employee &employee, const string &departmentId, const double &salary);
	void printDepartment(const string &departmentId);
	void changeSalary(string employeeId, double salary);
	void printSalaries();
	void printSalariesSorted();//malejaco
};

