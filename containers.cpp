#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <deque>
#include <string>
#include <iterator>
#include <utility> 
#include <algorithm>
#include <numeric>
#include <functional> 
#include <iomanip>
#include <set>
#include "containers.h"

using namespace std;

Employee::Employee(const string id, const string name, const string surname, const string departmentId, const string position) {
 
	this->id = id;
	this->name = name;
	this->surname = surname;
	this->departmentId = departmentId;
	this->position = position;
};

Employee::~Employee() {
	cout << "Destructor: \t" << this->name << " "<< this->surname << endl;
}

string Employee::getID() const { return this->id; }
string Employee::getname() const { return this->name; }
string Employee::getsurname() const { return this->surname; }
string Employee::getdepartmentId() const { return this->departmentId; }
string Employee::getposition() const { return this->position; }

void HRMS::add(Employee &employee, const string& departmentId, const double& salary) {
	AllEmployees.push_back(employee);
	mapList.insert(make_pair(employee.getID(), departmentId));
	mapSalary.insert(make_pair(employee.getID(), salary));
}

void HRMS::printDepartment(const string& departmentId) {

	map <const string, string>::iterator itrmapList;

	cout << setw(16) << left << "ID Number";
	cout << setw(20) << "Department"<< endl;

	cout << setw(16) << left << "----";
	cout << setw(20) << "------------" << endl;

	for (itrmapList = mapList.begin(); itrmapList != mapList.end();++itrmapList)
	{	
		if (itrmapList->second == departmentId)
		{
			cout << setw(16) << left << itrmapList->first << setw(20) << itrmapList->second << endl;
		};
	}
	cout << '\n';
}

void HRMS::changeSalary(string employeeId, double salary) {

	map <const string, double>::iterator itrSalary;

	cout << setw(16) << left << "ID Number";
	cout << setw(16) << "Salary" << endl;

	cout << setw(16) << left << "----";
	cout << setw(16) << "------------" << endl;

	for (itrSalary = mapSalary.begin(); itrSalary != mapSalary.end(); ++itrSalary)
	{
		if (itrSalary->first==employeeId)
		{
			auto access = mapSalary.insert(make_pair(itrSalary->first,salary));
			if (!access.second)
			{
				access.first->second = salary;
			}
			cout << setw(16) << left << itrSalary->first << setw(16) << itrSalary->second << endl;
			break;
		}
	};
	cout << '\n';
}

void HRMS::printSalaries() {
	cout << setw(16) << left << "ID Number";
	cout << setw(16) << "Name";
	cout << setw(16) << "Surname";
	cout << setw(20) << "DepartmentId";
	cout << setw(16) << "Position";
	cout << setw(16) << "Salary" << endl;

	cout << setw(16) << left << "----";
	cout << setw(16) << "------------";
	cout << setw(16) << "------------";
	cout << setw(20) << "------------";
	cout << setw(16) << "------------";
	cout << setw(16) << "------------" << endl;

	deque<Employee>::iterator itrAll;
	map <const string, double>::iterator itrSalary = begin(mapSalary);

	for (itrAll = AllEmployees.begin(); itrAll != AllEmployees.end(); ++itrAll)
	{
		cout << setw(16) << left << (*itrAll).getID() << setw(16) << (*itrAll).getname();
		cout << setw(16) << (*itrAll).getsurname() << setw(20) << (*itrAll).getdepartmentId();
		cout << setw(16) << (*itrAll).getposition() << setw(16)<<(*itrSalary).second << endl;
		++itrSalary;
	};
	cout << '\n';
}

void HRMS::printSalariesSorted() {

	cout << setw(16) << left << "ID Number";
	cout << setw(16) << "Name";
	cout << setw(16) << "Surname";
	cout << setw(20) << "DepartmentId";
	cout << setw(16) << "Position";
	cout << setw(16) << "Salary" << endl;

	cout << setw(16) << left << "----";
	cout << setw(16) << "------------";
	cout << setw(16) << "------------";
	cout << setw(20) << "------------";
	cout << setw(16) << "------------";
	cout << setw(16) << "------------" << endl;

	deque<Employee>::iterator itrAll= AllEmployees.begin();;
	deque<pair< string, double>> MyDeq;
	map < string, double>::iterator itrSalary= mapSalary.begin();

	for (itrSalary = mapSalary.begin(); itrSalary!=mapSalary.end(); ++itrSalary)
	{
		MyDeq.push_back(make_pair(itrSalary->first, itrSalary->second));
	}

	sort(MyDeq.begin(), MyDeq.end(), [](pair< string, double>& a, pair< string, double>& b){return (a.second > b.second); });
	
	for (int i = 0; i < MyDeq.size(); i++) {
		for (itrAll = AllEmployees.begin(); itrAll != AllEmployees.end(); ++itrAll)
		{
			if (MyDeq[i].first == (*itrAll).getID())
			{
				cout << setw(16) << left << (*itrAll).getID() << setw(16) << (*itrAll).getname();
				cout << setw(16) << (*itrAll).getsurname() << setw(20) << (*itrAll).getdepartmentId();
				cout << setw(16) << (*itrAll).getposition() << setw(16) << MyDeq[i].second << endl;
			}
		}
	};
	cout << '\n';
}
