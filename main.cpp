#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <deque>
#include <string>
#include <iterator>
#include <utility> 
#include <algorithm>
#include <numeric>
#include <functional> 
#include <iomanip>
#include <set>
#include "containers.h"

using namespace std;

int main()
{
	const int NUM = 10;

	Employee pracownik[NUM] = {
		Employee("0", "Jon1", "Smith1", "HR", "Associate1"),
		Employee("1", "Jon2", "Smith2", "Accounting", "Associate1"),
		Employee("2", "Jon3", "Smith3", "Compliance", "Associate1"),
		Employee("3", "Jon4", "Smith4", "HR", "Associate2"),
		Employee("4", "Jon5", "Smith5", "Accounting", "Associate2"),
		Employee("5", "Jon6", "Smith6", "Compliance", "Associate2"),
		Employee("6", "Jon7", "Smith7", "HR", "Team Lider"),
		Employee("7", "Jon8", "Smith8", "Accounting", "Team Lider"),
		Employee("8", "Jon9", "Smith9", "Compliance", "Team Lider"),
		Employee("9", "Jon10", "Smith10", "Head of Department", "Officer")
	};

	HRMS workers;

	workers.add(pracownik[0], "HR", 2000);
	workers.add(pracownik[1], "Accounting", 2000);
	workers.add(pracownik[2], "Compliance", 2000);
	workers.add(pracownik[3], "HR", 3000);
	workers.add(pracownik[4], "Accounting", 3000);
	workers.add(pracownik[5], "Compliance", 3000);
	workers.add(pracownik[6], "HR", 5000);
	workers.add(pracownik[7], "Accounting", 5000);
	workers.add(pracownik[8], "Compliance", 5000);
	workers.add(pracownik[9], "Head of Department", 8000);
	
	cout << "\n\nWypisuje pracownikow danego departamentu\n" << endl;
	workers.printDepartment("HR");

	cout << "\n\nZmieniam wyplate wskazanemu pracownikowi\n" << endl;
	workers.changeSalary("4",9000);

	cout << "\n\nWypisuje wszystkich pracownikow z informacjami podstawowymi oraz wyplata (po wczesniejszej zmianie)\n" << endl;
	workers.printSalaries();

	cout << "\n\nWypisuje wszystkich pracownikow z informacjami podstawowymi oraz wyplata - sortowanie po pensji -> malejaco\n" << endl;
	workers.printSalariesSorted();
	
	getchar();
	return 0;
}
